package de.geocv.webframe.webframework

import android.content.Context
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.webkit.JavascriptInterface
import android.widget.Toast


class JavaScriptReceiver
/** Instantiate the receiver and set the context  */
internal constructor(internal var mContext: Context) {

    @JavascriptInterface
    fun openDialog() {
        Toast.makeText(mContext,"From index1.html",Toast.LENGTH_LONG).show()
    }

}