package de.geocv.webframe.webframework

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_main.view.*
import android.webkit.WebChromeClient
import de.geocv.webframe.webframework.R.id.webView



/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_main, container, false)

        rootView.webView.loadUrl(getString(R.string.tab_url, arguments?.getInt(ARG_SECTION_NUMBER)))
        //rootView.webView.loadUrl(getString(R.string.tab_url_1))
        val javaScriptReceiver:JavaScriptReceiver = JavaScriptReceiver(requireContext());
        rootView.webView.addJavascriptInterface(javaScriptReceiver, "JSReceiver");
        rootView.webView.webChromeClient = WebChromeClient();
        rootView.webView.settings.javaScriptEnabled = true
        rootView.webView.settings.domStorageEnabled = true
        return rootView
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int): PlaceholderFragment {
            val fragment = PlaceholderFragment()
            val args = Bundle()
            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }
}